/*const API_REQUEST_URL = "https://min-api.cryptocompare.com/data/pricemulti";
const API_FROM_VALUES_PARAM = "fsyms";
const API_TO_VALUES_PARAM = "tsyms";
const API_UPADATE_INTERVAL_MS = 5000;*/
import axios from "axios";

const API_CURRENCY_LIST_URL = "https://min-api.cryptocompare.com/data/all/coinlist?summary=true";

const MAIN_CURRENCY = "USD";
const SECOND_CURRENCY = "BTC";
const AGREGATE_INDEX = "5";
const SUB_FAIL_INDEX = "500";
const API_MESSAGE_INVALID_SUB = "INVALID_SUB";

const tickersSubscriptions = new Map();

let defaultExchangePrice: number | null = null;

const sWorker = new SharedWorker('/assets/workers/sWorker.js', {type: 'module'});
subscribeOnDefaultExchangePair();

sWorker.port.onmessage = e => {
    const {TYPE: type, FROMSYMBOL: currency, PRICE: newPrice, MESSAGE: message, PARAMETER: parameter} = JSON.parse(e.data.swMessage);

    if (type === SUB_FAIL_INDEX && message === API_MESSAGE_INVALID_SUB) {
        const failedCurrencyPair = getFailedCurrencyFromMessage(parameter);
        if (!failedCurrencyPair) {
            return;
        }

        if (failedCurrencyPair.to === SECOND_CURRENCY) {
            const errorHandlers = tickersSubscriptions.get(failedCurrencyPair.from).errorCallbacks ?? [];
            errorHandlers.forEach((handler: (arg0: any) => any) => handler(message));
            return;
        }
        unsubscribeFromTickerOnWs(failedCurrencyPair.from, null);
        subscribeToTickerOnWs(failedCurrencyPair.from, SECOND_CURRENCY);
        tickersSubscriptions.get(failedCurrencyPair.from).subFailed = true;
        return;
    }

    if (type !== AGREGATE_INDEX) {
        return;
    }

    if (newPrice === undefined) {
        return;
    }

    if (currency === SECOND_CURRENCY) {
        defaultExchangePrice = newPrice;
    }

    if (!tickersSubscriptions.get(currency)) {
      return;
    }

    const handlers = tickersSubscriptions.get(currency).callbacks ?? [];
    const isSubFailed = tickersSubscriptions.get(currency).subFailed;
    handlers.forEach((handler: (arg0: any) => any) => handler(isSubFailed ? convertPriceForSecondaryExchange(newPrice) : newPrice));
}

function convertPriceForSecondaryExchange(price: number) {
    return defaultExchangePrice ? price * defaultExchangePrice : null;
}

function getFailedCurrencyFromMessage(message: string) {
    const matchResult = new RegExp("5~CCCAGG~(.*)~(.*)").exec(message);
    if (matchResult) {
        return {
            from: matchResult[1],
            to: matchResult[2]
        }
    }
    return null;
}

function subscribeOnDefaultExchangePair () {
    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${SECOND_CURRENCY}~${MAIN_CURRENCY}`]
    })
}

function subscribeToTickerOnWs(tickerName: string, toCurrency: string | null) {
    sendToWebSocket({
        action: "SubAdd",
        subs: [`5~CCCAGG~${tickerName}~${toCurrency ? toCurrency : MAIN_CURRENCY}`]
    })
}

function unsubscribeFromTickerOnWs(tickerName: string, toCurrency: string | null) {
    sendToWebSocket({
        action: "SubRemove",
        subs: [`5~CCCAGG~${tickerName}~${toCurrency ? toCurrency : MAIN_CURRENCY}`]
    })
}

function sendToWebSocket(message: { action: string; subs: string[]; }) {
    sWorker.port.postMessage(message);
}

export const subscribeToTickerUpdate = (tickerName: string, callback: any, onErrorCallback: any) => {
    const subscribers = tickersSubscriptions.get(tickerName) || {
        callbacks: [],
        errorCallbacks: [],
        subFailed: false
    };
    tickersSubscriptions.set(tickerName, {
        callbacks: [...subscribers.callbacks, callback],
        errorCallbacks: [...subscribers.errorCallbacks, onErrorCallback],
        subFailed: subscribers.subFailed
    });
    subscribeToTickerOnWs(tickerName, null);
}

export const unsubscribeFromTickerUpdate = (tickerName: string) => {
    if (tickersSubscriptions.get(tickerName).subFailed) {
        unsubscribeFromTickerOnWs(tickerName, SECOND_CURRENCY);
    } else {
        unsubscribeFromTickerOnWs(tickerName, null);
    }
    tickersSubscriptions.delete(tickerName);
}

export const getCurrencyList = async () => {
    let currencyList = null;
    await axios.get(API_CURRENCY_LIST_URL)
        .then(response => {
            currencyList = Object.values(response.data.Data).sort((a, b) => {
                // @ts-ignore
              if (a.Symbol > b.Symbol) {
                    return 1;
                }
                // @ts-ignore
              if (a.Symbol < b.Symbol) {
                    return -1;
                }
                return 0;
            });
        })
    return currencyList;
}
