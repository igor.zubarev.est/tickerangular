import { Ticker } from "../classes/interfaces";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class PersistenceApi {
  LOCAL_STORAGE_ID = "crypt-list"

  saveTickersStateToLocalStorage = (tickers: Ticker[]) => {
    if (localStorage) {
      localStorage.setItem(this.LOCAL_STORAGE_ID, JSON.stringify(tickers));
    }
  }

  getTickersStateFromLocalStorage = (): Ticker[] => {
    if (localStorage) {
      return <Ticker[]>JSON.parse(localStorage.getItem(this.LOCAL_STORAGE_ID) || '[]');
    }
    return []
  }
}
