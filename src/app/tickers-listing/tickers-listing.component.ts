import {Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {Ticker} from "../classes/interfaces";

@Component({
  selector: 'app-tickers-listing',
  templateUrl: './tickers-listing.component.html',
  styleUrls: ['./tickers-listing.component.scss']
})
export class TickersListingComponent implements OnInit, OnChanges{

  @Input() tickers: Ticker[] | undefined;
  @Input() unselectTicker: Boolean | undefined;

  @Output() tickerSelected = new EventEmitter<Ticker>();
  @Output() tickerUnselected = new EventEmitter();
  @Output() removeTicker = new EventEmitter<Ticker>();

  selectedTicker: Ticker | null = null

  constructor() {
  }

  handleSelect(tickerObject: Ticker) {
    if (!this.selectedTicker || this.selectedTicker.id !== tickerObject.id) {
      this.selectedTicker = tickerObject;
      this.tickerSelected.emit(this.selectedTicker);
    } else {
      this.selectedTicker = null;
      this.tickerUnselected.emit(this.selectedTicker);
    }
  }

  handleUnselect() {
    this.selectedTicker = null
  }

  handleRemove(event: any, tickerObject: Ticker) {
    event.stopPropagation();
    this.removeTicker.emit(tickerObject);
  }

  formatPrice(price: any) {
    if (price === "-" || !price) {
      return "-";
    }
    return price > 1 ? price.toFixed(2) : price.toPrecision(2);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['unselectTicker']) {
      let needUnselect = changes['unselectTicker'].currentValue
      if (needUnselect) {
        this.handleUnselect()
      }
    }
  }

  ngOnInit(): void {
  }

}
