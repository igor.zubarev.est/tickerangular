import {AfterViewInit, ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Ticker, PaginationState, FilterState} from "./classes/interfaces";
import {Router, ActivatedRoute} from "@angular/router";
import {PersistenceApi} from "./api/persistenceApi"
import {subscribeToTickerUpdate, unsubscribeFromTickerUpdate} from "./api/subscriptionsApi"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit{
  title = 'ticker-angular';

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private persistenceApi: PersistenceApi
  ) {}

  private tickerObjectsState: Ticker[] = [];
  selectedTickerPricesList: any[] = []
  paginatedTickers: Ticker[] = []
  filteredTickers: Ticker[] = []
  filterState: string = ""
  pageState: number = 1
  resetPagination: boolean = false
  pageLoaded = false;
  selectedTickerState: Ticker | null = null

  set tickerObjects(tickerObjects: Ticker[]) {
    this.tickerObjectsState = tickerObjects
    this.persistenceApi.saveTickersStateToLocalStorage(this.tickerObjects)
  }
  get tickerObjects(): Ticker[] {
    return this.tickerObjectsState
  }

  set selectedTicker(ticker: Ticker | null) {
    this.selectedTickerState = ticker
    this.selectedTickerPricesList = []
  }
  get selectedTicker(): Ticker | null {
    return this.selectedTickerState
  }

  set filter(filter: string) {
    this.filterState = filter
    this.resetPagination = true
    this.updateURLState()
  }
  get filter(): string {
    return this.filterState
  }

  set page (page: number) {
    this.pageState = page
    this.updateURLState()
  }
  get page(): number {
    return this.pageState
  }

  updateURLState() {
    const pageState = {}
    if (this.page > 1) {
      // @ts-ignore
      pageState.page = this.page
    }
    if (this.filter) {
      // @ts-ignore
      pageState.filter = this.filter
    }
    this.router.navigate([],{
      relativeTo: this.route,
      queryParams: pageState,
      queryParamsHandling: ""
    })
  }

  handlePagination(paginationState: PaginationState) {
    this.resetPagination = false;
    this.paginatedTickers = paginationState.paginatedTickers;
    this.page = paginationState.currentPage;
  }

  handleFilterUpdate(filterState: FilterState) {
    this.filter = filterState.filter;
    this.filteredTickers = filterState.filteredTickers;
  }

  handleAddTicker(newTickerObject: Ticker) {
    this.addTickerToStorage(newTickerObject);

    subscribeToTickerUpdate(newTickerObject.name,
      (newPrice: string) => this.updateTicker(newTickerObject.name, newPrice),
      (errorMessage: string) => this.markTickerWithError(newTickerObject.name, errorMessage)
    )
    this.cd.detectChanges()
  }

  removeTicker(ticker: Ticker) {
    this.removeTickerFromStorage(ticker);
    if (this.selectedTicker && this.selectedTicker.id === ticker.id) {
      this.selectedTicker = null;
    }
    unsubscribeFromTickerUpdate(ticker.name);
    this.cd.detectChanges()
  }

  removeTickerFromStorage(ticker: Ticker) {
    this.tickerObjects = this.tickerObjects.filter( t => t.id !== ticker.id)
  }

  updateTicker(tickerName: string, price: any) {
    this.tickerObjects.filter(t => t.name === tickerName).forEach(t => {
      t.price = price;
      if (this.selectedTicker && this.selectedTicker.id === t.id) {
        this.selectedTickerPricesList.push(price);
      }
    });
    this.cd.detectChanges()
  }

  markTickerWithError(tickerName: string, message: string) {
    const tickerWithError = this.tickerObjects.find(ticker => ticker.name === tickerName);
    if (tickerWithError) {
      tickerWithError.errorMsg = message;
    }
    this.cd.detectChanges()
  }

  addTickerToStorage(newTickerObject: Ticker) {
    this.tickerObjects = [...this.tickerObjects, newTickerObject]
  }

  setSelectedTicker(selectedTicker: Ticker | null) {
    this.selectedTicker = selectedTicker;
  }

  handleCloseGraph() {
    this.selectedTicker = null
  }

  ngOnInit() {
    this.tickerObjects = this.persistenceApi.getTickersStateFromLocalStorage();

    this.tickerObjects.forEach(ticker =>
      subscribeToTickerUpdate(ticker.name,
        (newPrice: string) => this.updateTicker(ticker.name, newPrice),
        (errorMessage: string) => this.markTickerWithError(ticker.name, errorMessage)
      )
    )

    const windowData = Object.fromEntries(new URL(window.location.toString()).searchParams.entries());
    if (windowData.page) {
      this.page = Number.parseInt(windowData.page)
    }
    if (windowData.filter) {
      this.filter = windowData.filter
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.pageLoaded = true;
    }, 500)

    this.cd.detectChanges()
  }
}
