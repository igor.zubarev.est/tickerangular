import {Component, Input, OnInit, Output, EventEmitter, OnChanges, AfterViewInit, SimpleChanges} from '@angular/core';
import {Ticker} from "../classes/interfaces";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit, OnChanges, AfterViewInit{

  @Input() tickersList!: Ticker[]
  @Input() filterInitialState!: string

  @Output() filterUpdated = new EventEmitter()

  set filter(filterString: string) {
    this.filterInitialState = filterString
    this.doFilter();
  }

  get filter() {
    return this.filterInitialState
  }

  doFilter() {
    const filterState = {
      filter: this.filter,
      filteredTickers: this.tickersList.filter(ticker => ticker.name.toLowerCase().includes(this.filter))
    }
    this.filterUpdated.emit(filterState);
  }

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["tickersList"]) {
      this.doFilter()
    }
  }

  ngAfterViewInit() {
    this.doFilter()
  }

}
