import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Ticker} from "../classes/interfaces";

@Component({
  selector: 'app-ticker-input',
  templateUrl: './ticker-input.component.html',
  styleUrls: ['./ticker-input.component.scss']
})
export class TickerInputComponent implements OnInit {

  @Input() existingTickers: Ticker[] | undefined
  @Output() addTickerEvent = new EventEmitter<Ticker>()

  cryptName = "";
  searchSuggestions: any[] = [];

  constructor() { }

  addTicker() {
    if (this.cryptName && this.isValidTicker) {
      const newTickerObject: Ticker = {
        id: new Date().getMilliseconds(),
        name: this.cryptName.toUpperCase(),
        price: "-",
        errorMsg: null
      }
      this.addTickerEvent.emit(newTickerObject);
      this.cryptName = "";
    }
  }

  handleSuggestionsUpdate(suggestions: any[]) {
    this.searchSuggestions = suggestions
  }

  handleSuggestionSelection(tickerSymbol: string) {
    if (tickerSymbol) {
      this.cryptName = tickerSymbol;
      this.addTicker();
    }
  }

  get isValidTicker() {
    let isTickerExist = false;
    if (this.existingTickers) {
      this.existingTickers.forEach((element) => {
        if (element.name.toLowerCase() === this.cryptName.toLowerCase()) {
          isTickerExist = true;
        }
      })
    }
    return !isTickerExist;
  }

  ngOnInit(): void {
  }

}
