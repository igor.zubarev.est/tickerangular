import {Component, OnInit, Input, Output, EventEmitter, OnChanges, AfterViewInit, SimpleChanges} from '@angular/core';
import { getCurrencyList } from "src/app/api/subscriptionsApi";

@Component({
  selector: 'app-search-suggestions',
  templateUrl: './search-suggestions.component.html',
  styleUrls: ['./search-suggestions.component.scss']
})
export class SearchSuggestionsComponent implements OnInit, OnChanges, AfterViewInit{

  @Input() searchValue: string | undefined;
  @Output() suggestion = new EventEmitter();
  @Output() suggestionSelected = new EventEmitter<string>();

  searchSuggestions: any[] = []
  currencyList: any[] | null = []
  searchTimerId: number | null = null
  breakSearch: boolean = true

  constructor() { }

  selectSuggestion(tickerSymbol: Symbol) {
    this.suggestionSelected.emit(tickerSymbol.toString());
  }

  handleSearchSuggestions(searchValue: string | undefined) {
    if (this.searchTimerId) {
      clearTimeout(this.searchTimerId);
      this.breakSearch = true;
    }
    if (searchValue && searchValue.length >= 2) {
      this.searchTimerId = setTimeout(() => {
        this.breakSearch = false;
        this.searchTicker(searchValue, 4);
      }, 500);
    }
  }

  searchTicker(searchString: string, searchLimit: number) {
    if (this.currencyList && this.currencyList.length) {
      const resultArray = [];
      for (let i = 0; i <= this.currencyList.length; i++) {
        if (resultArray.length >= searchLimit || this.breakSearch) {
          break;
        }
        let element = this.currencyList[i];
        if (element && element.Symbol.search(new RegExp(searchString, 'i')) !== -1) {
          resultArray.push(element);
        }
      }
      for (let i = 0; i <= this.currencyList.length; i++) {
        if (resultArray.length >= searchLimit || this.breakSearch) {
          break;
        }
        let element = this.currencyList[i];
        if (element && element.FullName.search(new RegExp(searchString, 'i')) !== -1) {
          if (resultArray.indexOf(element) === -1) {
            resultArray.push(element);
          }
        }
      }
      this.searchSuggestions = resultArray;
      this.suggestion.emit(this.searchSuggestions);
    }
  }

  async loadCurrencyListFromApi() {
    this.currencyList = await getCurrencyList();
  }



  ngOnInit(): void {
    this.loadCurrencyListFromApi();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["searchValue"]) {
      if (!this.searchValue) {
        this.searchSuggestions = [];
      }
      this.handleSearchSuggestions(this.searchValue);
    }
  }

  ngAfterViewInit() {
    if (this.searchValue) {
      this.handleSearchSuggestions(this.searchValue);
    }
  }

}
