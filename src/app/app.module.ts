import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilterComponent } from './filter/filter.component';
import { GraphComponent } from './graph/graph.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SearchSuggestionsComponent } from './search-suggestions/search-suggestions.component';
import { TickerInputComponent } from './ticker-input/ticker-input.component';
import { TickersListingComponent } from './tickers-listing/tickers-listing.component';
import { DefaultButtonComponent } from './stock/default-button/default-button.component';
import {PersistenceApi} from "./api/persistenceApi";

@NgModule({
  declarations: [
    AppComponent,
    FilterComponent,
    GraphComponent,
    PaginationComponent,
    SearchSuggestionsComponent,
    TickerInputComponent,
    TickersListingComponent,
    DefaultButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [PersistenceApi],
  bootstrap: [AppComponent]
})
export class AppModule { }
