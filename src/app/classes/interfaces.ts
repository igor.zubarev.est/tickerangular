export interface Ticker {
  errorMsg: string | null
  id: number
  name: string
  price: any
}

export interface PaginationState {
  paginatedTickers: Ticker[]
  currentPage: number
}

export interface FilterState {
  filter: string;
  filteredTickers: Ticker[]
}
