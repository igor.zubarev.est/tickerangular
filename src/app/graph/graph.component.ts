import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
  ViewChild,
  ElementRef, AfterViewInit
} from '@angular/core';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit, OnDestroy, AfterViewInit{
  @ViewChild('graphHolder') graphHolder!: ElementRef<HTMLInputElement>

  @Input() graphValues!: Array<number>
  @Input() fromCurrencyName!: string | null
  @Input() toCurrencyName!: string

  @Output() graphClosed = new EventEmitter()

  maxGraphSize = 0

  closeGraph() {
    this.graphClosed.emit();
  }
  setMaxGraphSize() {
    this.maxGraphSize = Math.floor(this.graphHolder.nativeElement.offsetWidth / 40);
  }

  get normalizedGraph() {
    const maxValue = Math.max(...this.graphValues);
    const minValue = Math.min(...this.graphValues);
    const minShowIndex = this.maxGraphSize < this.graphValues.length ? this.graphValues.length - this.maxGraphSize : 0;
    const maxShowIndex = this.graphValues.length;
    if (maxValue === minValue) {
      return this.graphValues.slice(minShowIndex, maxShowIndex).map(() => 50);
    }
    return this.graphValues
      .slice(minShowIndex, maxShowIndex)
      .map(
        price => 5 + ((price - minValue) * 95) / (maxValue - minValue)
      );
  }

  setBarStyle(bar: number) {
    return {
     'height': bar + '%'
    }
  }


  constructor() { }

  ngAfterViewInit() {
    this.setMaxGraphSize();
    window.addEventListener("resize", () => this.setMaxGraphSize());
  }

  ngOnDestroy() {
    window.removeEventListener("resize", () => this.setMaxGraphSize())
  }

  ngOnInit(): void {
  }

}
