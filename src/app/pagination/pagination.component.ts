import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ChangeDetectorRef} from '@angular/core';
import {PaginationState, Ticker} from "../classes/interfaces";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges, AfterViewInit {

  @Input() tickersLimit!: number
  @Input() tickersList!: Ticker[]
  @Input() initialPageState!: number

  @Output() paginationUpdated = new EventEmitter<PaginationState>()

  get paginatedTickers() {
    const pageStartIndex = (this.page - 1) * this.tickersLimit
    const pageEndIndex = this.page * this.tickersLimit
    return this.tickersList.slice(pageStartIndex, pageEndIndex)
  }

  get hasNextPage() {
    return this.tickersList.length > this.page * this.tickersLimit
  }

  get pageAmount() {
    return Math.ceil(this.tickersList.length / this.tickersLimit)
  }

  set page(page: number) {
    this.initialPageState = page
    this.serviceUpdates()
  }

  get page() {
    return this.initialPageState
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["tickersList"]) {
      this.serviceUpdates()
    }
  }

  serviceUpdates() {
    if (this.paginatedTickers.length === 0 && this.page > 1) {
      this.page -= 1;
    } else {
      this.paginationUpdated.emit( {
          paginatedTickers: this.paginatedTickers,
          currentPage: this.page
        }
      )
    }
  }

  constructor() { }

  ngAfterViewInit() {
    this.paginationUpdated.emit( {
        paginatedTickers: this.paginatedTickers,
        currentPage: this.page
      }
    )
  }

  ngOnInit(): void {
  }

}
